// Mini Activity:

class Person {

	constructor(name, age, nationality, address) {
		this.name = name;
		if (typeof age === "number" && age >= 18) {
			this.age = age;
		} else {
			this.age = undefined;
		}
		this.nationality = nationality;
		this.address = address;
	}
}

let personOne = new Person("Dennis", 15, "Filipino", "NCR");
let personTwo = new Person("Dennis", 18, "Filipino", "NCR");




// Quiz:

// 1. CLass
// 2. First Letter is Uppercased
// 3. new
// 4. instantiation
// 5. constructor

// Function Coding:

// 1.

class Student {

	constructor(name, email, grades) {
		this.passedWithHonors = undefined;
		this.passed = undefined;
		this.gradeAve = undefined;
		this.name = name;
		this.email = email;
		if (Array.isArray(grades) && grades.length === 4 && grades.every((score) => typeof score === "number" && score >= 0 && score <= 100)) {
      		this.grades = grades;
    	} else {
      		this.grades = undefined;
    	}
	}
	// Methods
	login() {
        console.log(`${this.email} has logged in`);
        return this;
    }
    logout() {
        console.log(`${this.email} has logged out`);
        return this;
    }
    listGrades() {
       console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`); 
       return this;
    }
    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        this.gradeAve = sum / 4;
        return this;
    }
    willPass() {
         if (this.gradeAve >= 85) {
           this.passed = true; 
           return this;
        } else {
        this.passed = false;
        return this;
        }
    }
    willPassWithHonors() {
        if (this.passed === true && this.gradeAve >= 90) {
            this.passedWithHonors = true;
            return this;
        } else if (this.passed === true && this.gradeAve >= 85 && this.gradeAve < 90) {
           this.passedWithHonors = false;
           return this;
        } else {
            this.passedWithHonors = false;
            return this;
        }
    }


}

//let studentOne = new Student("John", "john@mail.com", [89, 84, 78, 88]);
//let studentOne = new Student("John", "john@mail.com", [101, 84, 78, 88]);
//let studentOne = new Student("John", "john@mail.com", [-10, 84, 78, 88]);
//let studentOne = new Student("John", "john@mail.com", ["hello", 84, 78, 88]);
//let studentOne = new Student("John", "john@mail.com", [84, 78, 88]);


// 2.

let studentOne = new Student("John", "john@mail.com", [89, 84, 78, 88]);
let studentTwo = new Student("Joe", "joe@mail.com", [78, 82, 79, 85]);
let studentThree = new Student("Jane", "jane@mail.com", [87, 89, 91, 93]);
let studentFour = new Student("Jessie", "jessie@mail.com", [91, 89, 92, 93]);




// Activity 2:
// Quiz 2:

// 1. No, should be outside
// 2. No, they are not separated by commas
// 3. Yes
// 4. Getter and Setter
// 5. return this (Obj)

// Function Coding 2:

